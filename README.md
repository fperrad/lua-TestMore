
lua-TestMore : an Unit Testing Framework
========================================

Introduction
------------

lua-TestMore is a port of the Perl5 module [Test::More](https://metacpan.org/pod/Test::Simple).

It uses the [Test Anything Protocol](https://en.wikipedia.org/wiki/Test_Anything_Protocol) as output,
that allows a compatibility with the Perl QA ecosystem.

It's an extensible framework. See [lua-TestAssertion](https://framagit.org/fperrad/lua-testassertion)
an extension which provides many Lua friendly assertions.

It allows a simple and efficient way to write tests (without OO style).

Some tests could be marked as TODO or skipped.

Errors could be fully checked with error_like().

Links
-----

The homepage is at <https://fperrad.frama.io/lua-TestMore/>,
and the sources are hosted at <https://framagit.org/fperrad/lua-TestMore/>.

Copyright and License
---------------------

Copyright (c) 2009-2023 Francois Perrad

This library is licensed under the terms of the MIT/X11 license, like Lua itself.

