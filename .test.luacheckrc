codes = true
read_globals = {
    -- Test.More
    'plan',
    'done_testing',
    'skip_all',
    'BAIL_OUT',
    'ok',
    'nok',
    'is',
    'isnt',
    'like',
    'unlike',
    'cmp_ok',
    'type_ok',
    'subtest',
    'pass',
    'fail',
    'require_ok',
    'eq_array',
    'is_deeply',
    'error_is',
    'error_like',
    'lives_ok',
    'diag',
    'note',
    'skip',
    'todo_skip',
    'skip_rest',
    'todo',
    -- Test.Tester
    'test_out',
    'test_err',
    'test_fail',
    'test_diag',
    'test_test',
    -- test suite
    'platform',
}

files['test/bail_out.lua'].ignore = { '122/os' }
files['test/skipall.lua'].ignore = { '122/os' }
files['test/skip.lua'].ignore = { '511' }
files['test/skip_rest.lua'].ignore = { '511' }
files['test/todo.lua'].ignore = { '511' }

