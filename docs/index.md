
# lua-TestMore

---

## Overview

lua-TestMore is a port of the Perl5 module
[Test::More](https://metacpan.org/pod/Test::Simple).

It uses the
[Test Anything Protocol](https://en.wikipedia.org/wiki/Test_Anything_Protocol)
as output, that allows a compatibility with the Perl QA ecosystem.
For example,
[prove](https://metacpan.org/dist/Test-Harness/view/bin/prove)
a basic CLI, or
[Smolder](https://metacpan.org/pod/Smolder)
a Web-based Continuous Integration Smoke Server.

It's an extensible framework. See [lua-TestAssertion](https://framagit.org/fperrad/lua-testassertion)
an extension which provides many Lua friendly assertions.

It allows a simple and efficient way to write tests (without OO style).

Some tests could be marked as **TODO** or **skipped**.

Errors could be fully checked with `error_like()`.

## References

Ian Langworth, chromatic,
[Perl Testing](https://www.oreilly.com/library/view/perl-testing-a/0596100922/)
O'Reilly, 2005

## Status

It's developed for Lua 5.1, 5.2, 5.3 & 5.4.

## Download

lua-TestMore source can be downloaded from
[Framagit](https://framagit.org/fperrad/lua-TestMore).

## Installation

The easiest way to install lua-TestMore is to use LuaRocks:

```sh
luarocks install lua-testmore
```

or manually, with:

```sh
make install
```

## The Lua Test Suite

Now, the test suite has its own repository on <https://fperrad.frama.io/lua-Harness/>.

## Copyright and License

Copyright &copy 2009-2023 Fran&ccedil;ois Perrad

This library is licensed under the terms of the MIT/X11 license,
like Lua itself.
